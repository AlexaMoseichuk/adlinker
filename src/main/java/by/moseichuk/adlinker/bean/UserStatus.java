package by.moseichuk.adlinker.bean;

public enum UserStatus {
    UNVERIFIED,
    VERIFIED,
    ARCHIVE
}
