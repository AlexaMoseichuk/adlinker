package by.moseichuk.adlinker.dao;

import by.moseichuk.adlinker.bean.SocialLink;

public interface SocialLinkDao extends Dao<SocialLink> {
}
